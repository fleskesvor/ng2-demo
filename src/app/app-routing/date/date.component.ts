import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {
  public dt: Date;// = new Date();
  public minDate: Date = void 0;
  public dateDisabled: {date: Date, mode: string}[];

  constructor() { }

  ngOnInit() {
  }

  getMoment() {
    return moment(this.dt).format("YYYY-MM-DD");
  }

}
