import { Component, Input } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './modal/modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  idSequence: number = 1;
  events: Array<any> = [];
  locale: any = {
    columnFormat: 'dddd',
    firstDay: 1,
    dayNames: ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag'],
    dayNamesShort: ['Søn', 'Man', 'Tirs', 'Ons', 'Tors', 'Fre', 'Lør'],
    slotLabelFormat: 'HH:mm',
    timeFormat: 'H:mm'
  };

  constructor(private modalService: NgbModal) {
    /*
      This isn't responsive, but at least we can set a sensible value on load.
      (768 and 992 are breakpoints for xs and sm in Twitter Bootstrap.)
    */
    if (window.innerWidth < 768)
      this.locale.columnFormat = 'ddd';
  }

  handleDayClick({date: date=null, jsEvent: jsEvent=null}) {
    // event, jsEvent, view

    let dow = date.day();
    let start = date.format("HH:mm");
    let end = date.add(1, 'hour').format("HH:mm");

    this.events.push({
      "id": this.idSequence++,
      "start": start,
      "end": end,
      "dow": [ dow ],
      "timeStamp": jsEvent.timeStamp
    });
  }

  handleEventResize({event: event=null, delta: delta=null, revertFunc: revertFunc=null}) {
    // event, delta, revertFunc, jsEvent, ui, view
    if (delta._days > 0)
      revertFunc();
    let curEvent = this.findEventById(event.id);
    curEvent.end = event.end.format("HH:mm");
  }

  handleEventDrop({event: event=null, delta: delta=null}) {
    // event, delta, revertFunc, jsEvent, ui, view

    let curEvent = this.findEventById(event.id);

    curEvent.dow.forEach((value, index, dows) => {
        let dow = value + delta._days;
        if (dow > 6) dow -= 7;
        if (dow < 0) dow += 7;
        dows[index] = dow;
    });

    curEvent.start = event.start.format("HH:mm");
    curEvent.end = event.end.format("HH:mm");
  }

  handleEventClick({calEvent: calEvent=null, jsEvent: jsEvent=null, view: view=null}) {
    // calEvent, jsEvent, view

    let event = this.findEventById(calEvent.id);
    let timeDiff = jsEvent.timeStamp - event.timeStamp;

    if (timeDiff < 100) return;

    const modalRef = this.modalService.open(ModalComponent);
    let day = this.locale.dayNames[calEvent.dow[0]];

    modalRef.componentInstance.day = day;
    modalRef.componentInstance.start = calEvent.start.format("HH:mm");
    modalRef.componentInstance.end = calEvent.end.format("HH:mm");

    modalRef.result.then((confirm: boolean) => {
      if (confirm) this.deleteEventById(calEvent.id);
    });
  }

  findEventById(id: number) {
    return this.events.find(
      event => event.id == id
    );
  }

  deleteEventById(id: number) {
    let index = this.events.findIndex(
      event => event.id == id
    );
    if (index >= 0) this.events.splice(index, 1);
  }
}
